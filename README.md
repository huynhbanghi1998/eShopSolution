# ASP.NET Core 3.1 project from NghiHB
## Technologies
- ASP.NET Core 3.1
- Entity Framework Core 3.1
## Install package
- Microsoft.EntityFrameworkCore.Tools
- Microsoft.EntityFrameworkCore.Design
- Microsoft.EntityFrameworkCore.SQLServer
- Microsoft.Extensions.Configuration (Install for eShopSolution.Data)
- Microsoft.Extensions.Configuration.FileExtensions (Install for eShopSolution.Data)
- Microsoft.Extensions.Configuration.Json (Install for eShopSolution.Data)

# How to config and run
# How to contribute