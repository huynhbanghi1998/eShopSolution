﻿using eShopSolution.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace eShopSolution.Data.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            // Seeding for AppConfig
            modelBuilder.Entity<AppConfig>().HasData(
                new AppConfig() { Key = "HomeTitle", Value = "This is home page of eShopSolution" },
                new AppConfig() { Key = "HomeKeyword", Value = "This is keyword of eShopSolution" },
                new AppConfig() { Key = "HomeDescription", Value = "This is description of eShopSolution" });

            // Seeding for Languages
            modelBuilder.Entity<Language>().HasData(
                new Language() { Id = "vi-VN", Name = "Tiếng Việt", IsDefault = true },
                new Language() { Id = "en-US", Name = "English", IsDefault = true });

            // Seeding for Contacts
            modelBuilder.Entity<Contact>().HasData(
                new Contact() { Id = 1, Name = "Huỳnh Bá Nghi", Email = "huynhbanghi1998@gmail.com", PhoneNumber = "0979622203", Status = Enums.Status.Active, Message = "Huỳnh Bá Nghi" },
                new Contact() { Id = 2, Name = "Huỳnh Bá Phước", Email = "huynhbaphuoc1993@gmail.com", PhoneNumber = "0327922398", Status = Enums.Status.Active, Message = "Huỳnh Bá Phước" });

            // Seeding for Category
            modelBuilder.Entity<Category>().HasData(
                    new Category() { Id = 1, SortOrder = 1, IsShowOnHome = 1, Status = Enums.Status.Active, ParentId = null },
                    new Category() { Id = 2, SortOrder = 2, IsShowOnHome = 1, Status = Enums.Status.Active, ParentId = null });

            // Seeding for Category Translation
            modelBuilder.Entity<CategoryTranslation>().HasData(
                new CategoryTranslation()
                {
                    Id = 1,
                    CategoryId = 1,
                    Name = "Áo nam",
                    LanguageId = "vi-VN",
                    SeoAlias = "ao-nam",
                    SeoDescription = "Áo sơ mi nam trắng Việt Tiến",
                    SeoTitle = "Áo sơ mi nam trắng Việt Tiến"
                },
                new CategoryTranslation()
                {
                    Id = 2,
                    CategoryId = 1,
                    Name = "Man shirt",
                    LanguageId = "en-US",
                    SeoAlias = "man-shirt",
                    SeoDescription = "The white men shirt of Viet Tien",
                    SeoTitle = "The white men shirt of Viet Tien"
                },
                new CategoryTranslation()
                {
                    Id = 3,
                    CategoryId = 2,
                    Name = "Áo nữ",
                    LanguageId = "vi-VN",
                    SeoAlias = "ao-nu",
                    SeoDescription = "Sản phẩm áo thời trang nữ",
                    SeoTitle = "Sản phẩm áo thời trang nữ"
                },
                new CategoryTranslation()
                {
                    Id = 4,
                    CategoryId = 2,
                    Name = "Woman shirt",
                    LanguageId = "en-US",
                    SeoAlias = "woman-shirt",
                    SeoDescription = "The fashion woman shirt",
                    SeoTitle = "The fashion woman shirt"
                });

            // Seeding for Products
            modelBuilder.Entity<Product>().HasData(
                new Product()
                {
                    Id = 1,
                    Price = 200000,
                    OriginalPrice = 100000,
                    Stock = 0,
                    ViewCount = 0,
                    DateCreated = DateTime.Now,
                });

            modelBuilder.Entity<ProductTranslation>().HasData(
                new ProductTranslation()
                {
                    Id = 1,
                    ProductId = 1,
                    Name = "Áo sơ mi nam trắnng Việt Tiến",
                    Description = "Áo sơ mi nam trắnng Việt Tiến",
                    Details = "Áo sơ mi nam trắnng Việt Tiến",
                    SeoDescription = "Áo sơ mi nam trắnng Việt Tiến",
                    SeoTitle = "Áo sơ mi nam trắnng Việt Tiến",
                    SeoAlias = "ao-so-mi-nam-trang-viet-tien",
                    LanguageId = "vi-vn"
                },
                new ProductTranslation()
                {
                    Id = 2,
                    ProductId = 1,
                    Name = "The white men shirt of Viet Tien",
                    Description = "The white men shirt of Viet Tien",
                    Details = "The white men shirt of Viet Tien",
                    SeoDescription = "The white men shirt of Viet Tien",
                    SeoTitle = "The white men shirt of Viet Tien",
                    SeoAlias = "the-white-men-shirt-of-viet-tien",
                    LanguageId = "en-US"
                });

            // Seeding for ProductInCategory
            modelBuilder.Entity<ProductInCategory>().HasData(
                new ProductInCategory() { CategoryId = 1, ProductId = 1 });


            var roleId = new Guid("0AC90747-6C92-49FD-A350-22628E748E78");
            var userId = new Guid("E1F2BC29-F90F-4B84-896B-BC04396B6BBC");

            // Seeding for AppRole
            // any guid, but nothing is against to use the same one
            modelBuilder.Entity<AppRole>().HasData(new AppRole
            {
                Id = roleId,
                Name = "admin",
                NormalizedName = "admin",
                Description = "Administrator role"
            });

            // Seeding for AppUser
            var hasher = new PasswordHasher<AppUser>();
            modelBuilder.Entity<AppUser>().HasData(new AppUser
            {
                Id = userId,
                UserName = "admin",
                NormalizedUserName = "admin",
                Email = "huynhbanghi1998@gmail.com",
                NormalizedEmail = "huynhbanghi1998@gmail.com",
                EmailConfirmed = true,
                PasswordHash = hasher.HashPassword(null, "huynhbanghi2203"),
                SecurityStamp = string.Empty,
                FirstName = "Huynh",
                LastName = "Ba Nghi",
                Dob = new DateTime(1998, 3, 22)
            });

            // Seeding for AppUserRoles
            // Gán role admin cho appUser admin
            modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(new IdentityUserRole<Guid>
            {
                RoleId = roleId,
                UserId = userId
            });

        }
    }
}
