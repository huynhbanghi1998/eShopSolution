﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace eShopSolution.Data.Migrations
{
    public partial class SeedIdentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "OrderDate",
                table: "Orders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2022, 5, 26, 14, 50, 2, 244, DateTimeKind.Local).AddTicks(1319),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2022, 5, 26, 14, 21, 54, 398, DateTimeKind.Local).AddTicks(7823));

            migrationBuilder.InsertData(
                table: "AppRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { new Guid("0ac90747-6c92-49fd-a350-22628e748e78"), "09554890-6c36-4c85-af50-08441bf211b0", "Administrator role", "admin", "admin" });

            migrationBuilder.InsertData(
                table: "AppUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { new Guid("0ac90747-6c92-49fd-a350-22628e748e78"), new Guid("e1f2bc29-f90f-4b84-896b-bc04396b6bbc") });

            migrationBuilder.InsertData(
                table: "AppUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Dob", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { new Guid("e1f2bc29-f90f-4b84-896b-bc04396b6bbc"), 0, "b7246677-a344-414e-8c73-5ecbf510b2d7", new DateTime(1998, 3, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "huynhbanghi1998@gmail.com", true, "Huynh", "Ba Nghi", false, null, "huynhbanghi1998@gmail.com", "admin", "AQAAAAEAACcQAAAAEHLjbdxzVXb4uLNy/SJmZujmgiq4XXSdgBP8CiqETj+KYv9SSNH3Lbs+23vjwZ3rHg==", null, false, "", false, "admin" });

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2022, 5, 26, 14, 50, 2, 274, DateTimeKind.Local).AddTicks(4935));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AppRoles",
                keyColumn: "Id",
                keyValue: new Guid("0ac90747-6c92-49fd-a350-22628e748e78"));

            migrationBuilder.DeleteData(
                table: "AppUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { new Guid("0ac90747-6c92-49fd-a350-22628e748e78"), new Guid("e1f2bc29-f90f-4b84-896b-bc04396b6bbc") });

            migrationBuilder.DeleteData(
                table: "AppUsers",
                keyColumn: "Id",
                keyValue: new Guid("e1f2bc29-f90f-4b84-896b-bc04396b6bbc"));

            migrationBuilder.AlterColumn<DateTime>(
                name: "OrderDate",
                table: "Orders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2022, 5, 26, 14, 21, 54, 398, DateTimeKind.Local).AddTicks(7823),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2022, 5, 26, 14, 50, 2, 244, DateTimeKind.Local).AddTicks(1319));

            migrationBuilder.UpdateData(
                table: "Products",
                keyColumn: "Id",
                keyValue: 1,
                column: "DateCreated",
                value: new DateTime(2022, 5, 26, 14, 21, 54, 432, DateTimeKind.Local).AddTicks(5347));
        }
    }
}
